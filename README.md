 
 
 # Clone the respoitory first 
 
 1. In teh repository we have a file VirtualenvSetup.txt that you will need in the next step 
 2. It is referenced as 'requirements.txt' 
 
 
 # Setup the VirtualEnv 
 
 1. First you need to setup your own virtual environment with *$virtualenv <env_name>*
 2. Then activate your virtual environment *$ source <env_name>/bin/activate* 
 3. Inside your virtual environment (can be seen that the name of your virtualenv is shown in brackets before $, run the command: 
 	(<env_name>)$ pip install -r path/to/requirements.txt
 4. Now all packages you need to start are installed   
 
 
 # start the development server 
 
 1. for starting the development server, run the command py -3 manage.py runserver 
 2. the above command is for windows, if it doesn't work, omit the  '-3' 
 3. depending on your OS this command can vary, for Unix/MacOS use 'python3' instead of py 
 4. as default the webserver is now started at 127.0.0.1:8000, enter this in your browser 
 5. you should now see the first page of the WebApp 
 
 # The app itself 
 
 1. on the first page the user has to select the programming language he wants to categorize the snippets
 2. after clicking on 'Start' he is redirected to the Categorizing page, where the swipe library is included 
 3. on the upper half, the code snippet shoudl be displayed later on 
 4. below inside the card actually the selected language is shown
 5. If you swipe now left or right, you are redirected to the same page (up to now) and see in the field where the language 
 	was shown previously a: 
	- 1 if you accepted (marked as secure) the code snippet
	- -1 if you rejected (marked as insecure) the code snippet 
 6. in the future after swiping the user shoudl be redirected to a different page, where the snippet from before is shown again, 
 	his decision, and statistics how other users decided 