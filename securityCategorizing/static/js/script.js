$(document).ready(function() {

    var animating = false;
    var cardsCounter = 0;
    var numOfCards = 1;
    var decisionVal = 80;
    var pullDeltaX = 0;
    var deg = 0;
    var $card, $cardReject, $cardLike;
  
    //moves the card 
    function pullChange() {
      animating = true;
      deg = pullDeltaX / 10;
      $card.css("transform", "translateX("+ pullDeltaX +"px) rotate("+ deg +"deg)");
  
      var opacity = pullDeltaX / 100;
      var rejectOpacity = (opacity >= 0) ? 0 : Math.abs(opacity);
      var likeOpacity = (opacity <= 0) ? 0 : opacity;
      $cardReject.css("opacity", rejectOpacity);
      $cardLike.css("opacity", likeOpacity);
    };
  
    //decides if the card was rejected or accepted
    function release() {
      //Added code for the form event 
      var form = document.forms['categorize'];
  

      if (pullDeltaX >= decisionVal) { //accepted 
        $card.addClass("to-right");
        form.elements["categorization"].value = 1; 
        form.submit(); 
      } else if (pullDeltaX <= -decisionVal) { //rejected
        $card.addClass("to-left");
        form.elements["categorization"].value = -1; 
        form.submit()
      }
  
      if (Math.abs(pullDeltaX) >= decisionVal) {
        $card.addClass("inactive");
  
        setTimeout(function() {
          $card.addClass("below").removeClass("inactive to-left to-right");
          cardsCounter++;
          if (cardsCounter === numOfCards) {
            cardsCounter = 0;
            $(".demo__card").removeClass("below");
          }
        }, 300);
      }
  
      if (Math.abs(pullDeltaX) < decisionVal) {
        $card.addClass("reset");
      }
  
      setTimeout(function() {
        $card.attr("style", "").removeClass("reset")
          .find(".demo__card__choice").attr("style", "");
  
        pullDeltaX = 0;
        animating = false;
      }, 300);
    };
  
    $(document).on("mousedown touchstart", ".demo__card:not(.inactive)", function(e) {
      if (animating) return;
  
      $card = $(this);
      $cardReject = $(".demo__card__choice.m--reject", $card);
      $cardLike = $(".demo__card__choice.m--like", $card);
      var startX =  e.pageX || e.originalEvent.touches[0].pageX;
  
      $(document).on("mousemove touchmove", function(e) {
        var x = e.pageX || e.originalEvent.touches[0].pageX;
        pullDeltaX = (x - startX);
        if (!pullDeltaX) return;
        pullChange();
      });
  
      $(document).on("mouseup touchend", function() {
        $(document).off("mousemove touchmove mouseup touchend");
        if (!pullDeltaX) return; // prevents from rapid click events
        release();
      });
    });
  
  });