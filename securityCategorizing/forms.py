from django import forms 
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from securityCategorizing.models import Language


class ChooseLanguageForm(forms.Form):
    #create our iterable List of languages depending on currently saved languages  
    languages = Language.objects.all()
    languageList= [] 
    for x in languages: 
        languageList.append((x, x))

    choosenLanguage = forms.ChoiceField(widget=forms.Select, choices=languageList )

    #TODO validate the input
    def clean_languages(self): 
        data = self.cleaned_data['choosenLanguage']
        languages = Language.objects.all() 
        if  languages.contains(data): 
            return data

class CategorizationResultForm(forms.Form): 
    categorization= forms.IntegerField()
    #code_id= forms.UUIDField()

    def clean_categorization(self): 
        category= self.cleaned_data['categorization']
        if category<-1 or category>1: 
            raise ValidationError(message="The categorization was manipulated", code= 'invalid')
        return category