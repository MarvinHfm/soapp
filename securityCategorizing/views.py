from django.shortcuts import render, get_object_or_404
from securityCategorizing.models import Language, CodeSnippet
from django.http import HttpResponseRedirect

from securityCategorizing.forms  import ChooseLanguageForm, CategorizationResultForm




# Create your views here.

def chooseLanguage(request): 
    """View function for the choose of your language""" 
    languages = Language.objects.all()
    context = {
        'languages' : languages
    }
    return render(request, 'chooseLanguage.html', context= context)


def categorizing (request): 
    """View function for the categorizing page"""
    #when arriving from the start page 
   
    
    if request.method == 'GET': 
        form = ChooseLanguageForm(request.GET)
        if form.is_valid(): 
            choosenLanguage = form.cleaned_data['choosenLanguage']#get the choosen language from the beginning 

        #TODO save the language (maybe in the session) to have it for upcoming requets

        #TODO call function to determine which code snippet to select 
        code= CodeSnippet.snippet #the selected text to display
        context = {
            'language' : choosenLanguage, 
            'code' : code,
        }

        return render(request, 'categorizing.html', context =context)
        
    elif  request.method == 'POST':  
        form = CategorizationResultForm (request.POST)        
        if form.is_valid(): 
            categorization= form.cleaned_data['categorization']

            """here i get 1 (for accepted cards) and -1 (from rejected cards) 
            i get 0 if no value was set 
            the values are set in the script.js depending on the direction the user swipes """
            #TODO modify the Database through increasing insecure/secure counter 
            #TODO give the codeUUID to know which snippet we were at
            #code_UUID = form.cleaned_data['code_id']
        else: 
            categorization= form.errors

        context = {
            'language' : categorization, 
            #'code' : code_UUID,
        }

        return render(request, 'categorizing.html', context =context)
    #TODO modify the cookie and depending on this choose the snippet to show 
    



"""Helper functions which shoudl be moved to seperate file later"""


     
    