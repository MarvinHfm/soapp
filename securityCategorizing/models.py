from django.db import models

# Create your models here.

class CodeSnippet (models.Model): 
    #Fields 
    id = models.UUIDField
    language=models.ForeignKey('Language', on_delete=models.SET_NULL, null=True) #or also include code snippets with more than one language 
    votesSecure= models.IntegerField(default=0)
    votesInsecure = models.IntegerField(default=0)
    votes = models.IntegerField(default=0)
    snippet = models.TextField(default="")
    #Metadata 

    #Methods
    def __str__(self):
        return "This is code snippet with ID: "+ self.id
    
    def __str__(self): 
        return self.snippet



class Language (models.Model): 
    name = models.CharField(max_length=20, primary_key=True)

    def __str__(self):
        return self.name


