from django.contrib import admin
from .models import CodeSnippet, Language


# Register your models here.
admin.site.register(CodeSnippet)
admin.site.register(Language)
