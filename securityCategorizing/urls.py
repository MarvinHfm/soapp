from django.urls import path
from . import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('', views.chooseLanguage, name='chooseLanguage'), #home page for choosing the language
    path('language/', views.categorizing, name='categorizing'), #home page for choosing the language
    path('categorize/', views.categorizing, name='categorizing'), #home page for choosing the language
] #+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) #for support of css and js files